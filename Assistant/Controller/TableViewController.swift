//
//  TableViewController.swift
//  Assistant
//
//  Created by Артем Оголец on 12/03/2019.
//  Copyright © 2019 Артем Оголец. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    var membershipsArray = ["1", "2", "3"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return membershipsArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MembershipCell", for: indexPath) as! MembershipViewCell
        
        return cell
    }

    
 

}
