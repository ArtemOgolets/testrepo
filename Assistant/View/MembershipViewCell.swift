//
//  MembershipViewCell.swift
//  Assistant
//
//  Created by Артем Оголец on 12/03/2019.
//  Copyright © 2019 Артем Оголец. All rights reserved.
//

import UIKit

class MembershipViewCell: UITableViewCell {
    
    @IBOutlet weak var membershipView: UIView!
    @IBOutlet weak var numberOfMembershipLabel: UILabel!
    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var fourthLabel: UILabel!
    @IBOutlet weak var fifthLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
